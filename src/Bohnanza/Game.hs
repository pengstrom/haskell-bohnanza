{-# LANGUAGE MultiParamTypeClasses #-}
module Bohnanza.Game
  ( module Bohnanza.Game
  , module Bohnanza.GameState
  ) where


import           Debug.Trace

import           System.Random
import           Control.Monad.State             (State, StateT, MonadState(..), runState, modify, gets, lift, replicateM)
import           Control.Monad.Random.Class      (MonadRandom(..))
import           Control.Monad.Coroutine
import           Control.Monad.Coroutine.SuspensionFunctors
           
import           Bohnanza.Cards                  (Bean)
import           Bohnanza.Deck                   (Deck, prepareDeck)
import qualified Bohnanza.Deck              as D
import qualified Bohnanza.Hands             as H
import           Bohnanza.Fields                 (score)
import           Bohnanza.Players                (Player(..), mkPlayer)
import qualified Bohnanza.Players           as P
import           Bohnanza.GameState

-- | Execute the game action given a handler in the IO monad.
runBohnanza :: (Reason -> GameState -> IO Response) -> IO ()
runBohnanza = runBohnanzaHelper emptyState start

runBohnanzaHelper :: GameState -> Bohnanza a -> (Reason -> GameState -> IO Response) -> IO ()
runBohnanzaHelper state (Bohnanza m) handler = do
  --traceM $ "\nStepping with state: " ++ show state
  let m' = resume m
      (cont, state') = runState m' state
  --traceM $ "With new state: " ++ show state'
  case cont of
    Left (Request reason f) -> do
      --traceM $ "With reason: " ++ show reason
      response <- handler reason state'
      --traceM $ "With response: " ++ show response
      let cont = f response
      runBohnanzaHelper state' (Bohnanza cont) handler
    Right x -> return ()

-- | Start the game.
start :: Bohnanza ()
start = do
  initStdGen
  initNames
  initDeck
  dealHands
  s <- get
  traceM $ "\n" ++ prettyState s

initStdGen :: Bohnanza ()
initStdGen = do
  GameSeed g <- report Seed
  modify $ \s -> s { rGen = g }

initDeck :: Bohnanza ()
initDeck = do
  n <- gets (length . gamePlayers)
  let n' = min 7 $ max 3 n
  Just deck <- prepareDeck n
  modify $ \s -> s { gameDeck = deck }

initNames :: Bohnanza ()
initNames = do
  PlayerNames names <- report Names
  let players = map mkPlayer names
  modify (\s -> s { gamePlayers = players })

drawCard :: Bohnanza Bean
drawCard = do
  deck <- gets gameDeck
  case D.drawCard deck of
    Just (bean, deck') -> do
      modify $ \s -> s { gameDeck = deck' }
      return bean
    Nothing -> do
      phase <- gets gamePhase
      if phase == 3
        then do
          report_ End
          return $ error "Terminate after 'End'!"
        else do
          ReapMany reapings <- report ReshuffleImminent
          reapMany reapings
          reshuffle
          drawCard

reap :: (Int, Int) -> Bohnanza ()
reap (n, m) = do
  players <- gets gamePlayers
  let player = players !! (n-1)
      fields = playerFields player
  case score fields m of
    Just (scored, discarded, fields') -> do
      currentDiscard <- gets discardDeck
      players <- gets gamePlayers
      let scored' = scored ++ playerScore player
          discarded' = discarded ++ currentDiscard
          player' = player
            { playerFields = fields'
            , playerScore = scored'
            }
          players' = replaceAt players player' (n-1)
      modify $ \s -> s { gamePlayers = players', discardDeck = discarded' }
    Nothing -> report_ (Error "Illegal field index")

reapMany :: [(Int, Int)] -> Bohnanza ()
reapMany = mapM_ reap

reshuffle :: Bohnanza ()
reshuffle = do
  deck <- gets gameDeck
  discard <- gets discardDeck
  deck' <- D.reshuffle deck discard
  modify $ \s -> s { gameDeck = deck', discardDeck = [] }

drawCards :: Int -> Bohnanza [Bean]
drawCards n = replicateM n drawCard

dealHand :: Int -> Int -> Bohnanza ()
dealHand 0 _ = return ()
dealHand n m = do
  players <- gets gamePlayers
  let player = players !! (m-1)
  bean <- drawCard
  let hand = playerHand player
      player' = player { playerHand = H.addCard hand bean } 
      players' = replaceAt players player' (m-1)
  modify $ \s -> s { gamePlayers = players' }
  dealHand (n-1) m 

dealHands :: Bohnanza ()
dealHands = do
  m <- gets $ length . gamePlayers
  mapM_ (dealHand 5) [1..m]

deal3 :: Int -> Bohnanza ()
deal3 = dealHand 3

replaceAt :: [a] -> a -> Int -> [a]
replaceAt (_:xs) x' 0 = x':xs
replaceAt (x:xs) x' n
  | n > 0 = x : replaceAt xs x' (n-1)
  | otherwise = error "Illegal replacement index"
