module Bohnanza.Cards where

-- All cards give 2 and 3 points, almost all 1 and 4
type CardScore = (Maybe Int,  Int,  Int,  Maybe Int)

data Bean
  = Coffee
  | Wax
  | Blue
  | Chili
  | Stink
  | Green
  | Soy
  | BlackEyed
  | Red
  | Garden
  | Cocoa
  deriving (Eq, Show)

-- Pretty card representation
prettyBean :: Bean -> String
prettyBean Coffee    = "Coffee Bean (24) - 4, 7, 10, 12"
prettyBean Wax       = "Wax Bean    (20) - 4, 7,  9, 11"
prettyBean Blue      = "Blue Bean   (20) - 4, 6,  8, 10"
prettyBean Chili     = "Chili Bean  (18) - 3, 6,  8,  9"
prettyBean Stink     = "Stink Bean  (16) - 3, 5,  7,  8"
prettyBean Green     = "Green Bean  (14) - 3, 5,  6,  7"
prettyBean Soy       = "Soy Bean    (12) - 2, 4,  6,  7"
prettyBean BlackEyed = "Coffee Bean (10) - 2, 4,  5,  6"
prettyBean Red       = "Red Bean    (8)  - 2, 3,  4,  5"
prettyBean Garden    = "Garden Bean (6)  - X, 2,  3,  X"
prettyBean Cocoa     = "Cocoa Bean  (4)  - X, 2,  3,  4"

-- Total number of cards of each type
beanAmount :: Bean -> Int
beanAmount Coffee    = 24
beanAmount Wax       = 22
beanAmount Blue      = 20
beanAmount Chili     = 18
beanAmount Stink     = 16
beanAmount Green     = 14
beanAmount Soy       = 12
beanAmount BlackEyed = 10
beanAmount Red       = 8
beanAmount Garden    = 6
beanAmount Cocoa     = 4

beanScore :: Bean -> CardScore
beanScore Coffee    = (Just  4, 7, 10, Just 12)
beanScore Wax       = (Just  4, 7,  9, Just 11)
beanScore Blue      = (Just  4, 6,  8, Just 10)
beanScore Chili     = (Just  3, 6,  8, Just  9)
beanScore Stink     = (Just  3, 5,  7, Just  8)
beanScore Green     = (Just  3, 5,  6, Just  7)
beanScore Soy       = (Just  2, 4,  6, Just  7)
beanScore BlackEyed = (Just  2, 4,  5, Just  6)
beanScore Red       = (Just  2, 3,  4, Just  5)
beanScore Garden    = (Nothing, 2,  3, Nothing)
beanScore Cocoa     = (Nothing, 2,  3, Just  4)
