{-# LANGUAGE NamedFieldPuns #-}
module Bohnanza.Players where


import           Text.Printf
import           Bohnanza.Cards       (Bean)
import           Bohnanza.Deck        (Deck)
import qualified Bohnanza.Deck   as D
import           Bohnanza.Fields      (Fields, mkFields, prettyFields)
import qualified Bohnanza.Fields as F
import           Bohnanza.Hands       (Hand, mkHand, takeCard, addCard)
import qualified Bohnanza.Hands  as H

import           Safe (headMay)


data Player = Player
  { playerHand   :: Hand
  , playerFields :: Fields
  , playerScore  :: Deck
  , playerName   :: String
  }
  deriving (Eq, Show)

mkPlayer :: String -> Player
mkPlayer name = Player
  { playerHand   = mkHand
  , playerFields = mkFields
  , playerScore  = []
  , playerName   = name
  }

prettyPlayer :: Player -> String
prettyPlayer Player{ playerHand, playerFields, playerScore, playerName }
  = printf "- Player %s -\n Hand (%d) %s\n Fields: %s\n Score (%d) %s\n"
      playerName
      (H.size playerHand) (show $ H.toList playerHand)
      (prettyFields playerFields)
      (length playerScore) (show playerScore)

activateThird :: Player -> Player
activateThird player@Player{ playerFields } = player { playerFields = F.activateThird playerFields}

firstCard :: Player -> Maybe (Bean, Player)
firstCard player = do
  let hand = playerHand player
  (bean, hand') <- takeCard hand
  let player' = player { playerHand = hand }
  return (bean, player')

drawCard :: Player -> Deck -> Maybe (Deck, Player)
drawCard player deck = do
  (bean, deck') <- D.drawCard deck
  let hand  = playerHand player
      hand' = addCard hand bean
      player' = player { playerHand = hand }
  return (deck', player')
