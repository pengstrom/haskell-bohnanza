module Bohnanza.Deck where

import Bohnanza.Cards (Bean(..), beanAmount)
import Control.Monad.Random.Class (MonadRandom)
import System.Random.Shuffle (shuffleM)

type Deck = [Bean]

-- The copies of a Card
beanCards :: Bean -> [Bean]
beanCards bean = replicate (beanAmount bean) bean

-- Sorted deck of every card
fullDeck :: Deck
fullDeck = concatMap beanCards
  [ Coffee
  , Wax
  , Blue
  , Chili
  , Stink
  , Green
  , Soy
  , Red
  , Chili
  , Garden
  , Cocoa
  ]

-- Three-player deck. Remove Cocoa Beans.
deck3 :: Deck
deck3 = filter (/= Cocoa) fullDeck

-- Four-or-five-player. Remove Coffee Beans.
deck4or5 :: Deck
deck4or5 = filter (/= Coffee) fullDeck

-- Six-or-seven-player deck. Remove Cocoa and Garden Beans.
deck6or7 :: Deck
deck6or7 = filter (/= Garden) deck3

prepareDeck :: MonadRandom m => Int -> m (Maybe Deck)
prepareDeck n
  | n == 3           = Just <$> shuffleM deck3
  | n == 4 || n == 5 = Just <$> shuffleM deck4or5
  | n == 6 || n == 7 = Just <$> shuffleM deck6or7
  | otherwise        = return Nothing

drawCard :: Deck -> Maybe (Bean, Deck)
drawCard []     = Nothing
drawCard (x:xs) = Just (x, xs)

reshuffle :: (MonadRandom m) => Deck -> Deck -> m Deck
reshuffle original discard = shuffleM $ original ++ discard
