module Bohnanza.Fields where

import Text.Printf
import Debug.Trace
import Safe (headDef)

import Bohnanza.Cards (Bean(..), CardScore, beanScore)
import Bohnanza.Deck (Deck)


type Field = (Bean, Int)

-- Fields may be empty. Third field may not be available.
type Fields = (Maybe Field, Maybe Field, Maybe (Maybe Field))


mkFields :: Fields
mkFields = (Nothing, Nothing, Nothing)

prettyField :: Maybe Field -> String
prettyField Nothing = "Empty"
prettyField (Just (bean, n)) = printf "(%d) %s" n (show bean)

prettyFields :: Fields -> String
prettyFields (f1, f2, Just f3)
  = printf "1: %s, 2: %s, 3: %s"
      (prettyField f1) (prettyField f2) (prettyField f3)
prettyFields (f1, f2, Nothing)
  = printf "1: %s, 2: %s"
      (prettyField f1) (prettyField f2)

-- May only plant on available empty fields or available fields of the same
-- type.
plant :: Fields -> Bean -> Int -> Maybe Fields
-- Empty fields
plant (Nothing, f2, f3) bean 1 = Just (Just (bean, 1), f2, f3)
plant (f1, Nothing, f3) bean 2 = Just (f1, Just (bean, 1), f3)
plant (f1, f2, Just Nothing) bean 3 = Just (f1, f2, Just (Just (bean, 1)))
-- Same-type fields
plant (Just (bean', n), f2, f3) bean 1
  | bean == bean' = Just (Just (bean, n+1), f2, f3)
  | otherwise     = Nothing
plant (f1, Just (bean', n), f3) bean 2
  | bean == bean' = Just (f1, Just (bean, n+1), f3)
  | otherwise     = Nothing
plant (f1, f2, Just (Just (bean', n))) bean 3
  | bean == bean' = Just (f1, f2, Just (Just (bean, n+1)))
  | otherwise     = Nothing
plant _ _ _ = Nothing

-- Scoring discards all but 'score' number of cards from the field.
score :: Fields -> Int -> Maybe (Deck, Deck, Fields)
score fs x
  | x >= 1 && x <= 3 = Just $ score' fs x
  | otherwise        = Nothing

score' :: Fields -> Int -> (Deck, Deck, Fields)
score' (f1, f2, f3) 1 = (scoreDeck, discardDeck, (Nothing, f2, f3))
  where
    (scoreDeck, discardDeck) = scoreField f1
score' (f1, f2, f3) 2 = (scoreDeck, discardDeck, (f1, Nothing, f3))
  where
    (scoreDeck, discardDeck) = scoreField f2
score' (f1, f2, Just f3') 3 = (scoreDeck, discardDeck, (f1, f2, Just Nothing))
  where
    (scoreDeck, discardDeck) = scoreField f3'
score' f _ = ([], [], f)

scoreField :: Maybe Field -> (Deck, Deck)
scoreField (Just (bean, n)) = (scoreDeck, discardDeck)
  where
    (mx, y, z, mw) = beanScore bean
    mw' = maybe False (<=n) mw
    z'  = n >= z
    y'  = n >= y
    mx' = maybe False (<=n) mx
    scores = map fst $ filter snd $ zip [4,3,2,1] [mw', z', y', mx']
    score = headDef 0 scores
    scoreDeck = replicate score bean
    discardDeck = replicate (n - score) bean
scoreField Nothing = ([], [])

-- Activate third bean field if not already activated
activateThird :: Fields -> Fields
activateThird (f1, f2, Nothing) = (f1, f2, Just Nothing)
activateThird fs = fs

thirdActivated :: Fields -> Bool
thirdActivated (_, _, Just _) = True
thirdActivated _              = False

fieldScore :: Fields -> (Int, Int, Int)
fieldScore fs = (length x, length y, length z)
  where
    Just (x, _, _) = score fs 1
    Just (y, _, _) = score fs 2
    Just (z, _, _) = score fs 3
