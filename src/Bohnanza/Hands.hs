module Bohnanza.Hands where

import Bohnanza.Cards (Bean)

newtype Hand = Hand [Bean]
    deriving (Eq, Show)

mkHand :: Hand
mkHand = Hand []

-- Add card to hand
addCard :: Hand -> Bean -> Hand
addCard (Hand hand) bean = Hand $ hand ++ [bean]

-- If possible, take first card from hand
takeCard :: Hand -> Maybe (Bean, Hand)
takeCard (Hand [])     = Nothing
takeCard (Hand (x:xs)) = Just (x, Hand xs)

size :: Hand -> Int
size (Hand xs) = length xs

toList :: Hand -> [Bean]
toList (Hand xs) = xs
