{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NamedFieldPuns #-}

module Bohnanza.GameState where

import           Text.Printf
import           System.Random
import           Control.Monad                   (void)
import           Control.Monad.State             (State, StateT, MonadState(..), runState, modify, gets, lift)
import           Control.Monad.Random.Class      (MonadRandom(..))
import           Control.Monad.Coroutine
import           Control.Monad.Coroutine.SuspensionFunctors
           
import           Bohnanza.Cards                  (Bean)
import           Bohnanza.Deck                   (Deck, prepareDeck)
import           Bohnanza.Players                (Player, mkPlayer)
import qualified Bohnanza.Players           as P

-- The game is modelled with a deck, a discard, some players and an offer
-- area.
data GameState = GameState
  { gameDeck    :: Deck
  , discardDeck :: Deck
  , gamePlayers :: [Player]
  , offerArea   :: Maybe (Bean, Bean)
  , rGen        :: StdGen
  , gamePhase   :: Int
  }
  deriving (Show)

data Reason
  = ReshuffleImminent
  | Names
  | Seed
  | Error String
  | End
  deriving (Eq, Show)

data Response
  = ReapMany [(Int, Int)]
  | PlayerNames [String]
  | GameSeed StdGen
  deriving (Show)

newtype Bohnanza a = Bohnanza (Coroutine (Request Reason Response) (State GameState) a) 
  deriving (Functor, Applicative, Monad)

instance MonadState GameState Bohnanza where
  get     = Bohnanza $ lift get
  put x   = Bohnanza $ lift $ put x
  state f = Bohnanza $ lift $ state f

instance MonadRandom Bohnanza where
  getRandomR lohi = do
    g <- gets rGen
    let (a, g') = randomR lohi g
    modify $ \s -> s { rGen = g' }
    return a
  getRandom = do
    g <- gets rGen
    let (a, g') = random g
    modify $ \s -> s { rGen = g' }
    return a
  getRandomRs lohi = do
    g <- gets rGen
    return $ randomRs lohi g
  getRandoms = do
    g <- gets rGen
    return $ randoms g

report :: Reason -> Bohnanza Response
report = Bohnanza . request

report_ :: Reason -> Bohnanza ()
report_ = void . report

emptyState :: GameState
emptyState = GameState
  { gameDeck    = []
  , discardDeck = []
  , gamePlayers = []
  , offerArea   = Nothing
  , rGen        = mkStdGen 0
  , gamePhase   = 1
  }

prettyState :: GameState -> String
prettyState GameState{ gameDeck, discardDeck, gamePlayers, offerArea, gamePhase }
  = printf
      "Deck (%d): %s\nDiscard (%d): %s\nPlayers:\n%s\nOffer area: %s\nPhase: %d\n"
      (length gameDeck) (show gameDeck)
      (length discardDeck) (show discardDeck)
      (concatMap P.prettyPlayer gamePlayers)
      (show offerArea)
      gamePhase
