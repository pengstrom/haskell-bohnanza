module Main where

import System.Random (newStdGen)
import Control.Monad.Coroutine
import Control.Monad.Coroutine.SuspensionFunctors

import Bohnanza.Game (GameState(..), Reason(..), Response(..), runBohnanza)

main :: IO ()
main = do
  putStrLn "Bohnanza Haskell game"
  runBohnanza handler
  putStrLn "Game ended"

handler :: Reason -> GameState -> IO Response
handler Names _ = do
    putStr "Enter player names: "
    names' <- getLine
    let names = words names'
    putStrLn $ "Got: " ++ show names
    return $ PlayerNames names
handler Seed _ = do
  g <- newStdGen
  return $ GameSeed g
